<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// T
	'theme_bsspruce_description' => 'Camping in the woods',
	'theme_bsspruce_slogan' => 'Camping in the woods',
);
